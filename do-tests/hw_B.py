# -*- coding: utf-8 -*-
"""
Created on Wed Dec 13 21:47:06 2017

@author: beall
"""
from __future__ import division

from thundersnow import ThunderSnow
import numpy as np


#Initial conditions
c = np.array([0.03,0.015,0.06,0])
areas = np.array([13.4, 12, 384.5, 4400])
V0 = np.array([0.26, 0.18, 0.68, 22])
h0 = 1000 * V0 / areas
Vout0 = c * np.sqrt(h0)
vin = [0.13,0.13,0.13,0.21,0.21,0.21,0.13,0.13,0.13,0.13,0.13,0.13,0.13]
Vin = [0,0,0,0]

#Initialize model
m = ThunderSnow()

#time array 
m.time = np.linspace(0,1,13)
#define constants
#ThunderSnow Constants exist
c = m.Array(m.Const,4,value=0)
c[0].value = 0.03
c[1].value = c[0] / 2
c[2].value = c[0] * 2
c[3].value = 0
#python constants are equivilant to ThunderSnow constants
Vuse = [0.03,0.05,0.02,0.00]

#Paramters
evap_c = m.Array(m.Param,4,value=1e-5)
evap_c[-1].value = 0.5e-5

A = [m.Param(value=i) for i in areas] #python list comprehension

Vin[0] = m.Param(value=vin)

#Variables
V = [m.Var(value=i) for i in V0]
h = [m.Var(value=i) for i in h0]
Vout = [m.Var(value=i) for i in Vout0]

#Intermediates
Vin[1:4] = m.Array(m.Inter,3)
[m.Inter_EQ(Vin[i+1] == Vout[i]) for i in range(3)]
Vevap = m.Array(m.Inter,4)
[m.Inter_EQ(Vevap[i] == evap_c[i] * A[i]) for i in range(4)]

#Equations
m.Equations([V[i].dt() == Vin[i] - Vout[i] - Vevap[i] - Vuse[i] for i in range(4)])
m.Equations([1000*V[i] == h[i]*A[i] for i in range(4)])
m.Equations([Vout[i]**2 == c[i]**2 * h[i] for i in range(4)])
  
  
#Set to simulation mode
m.options.imode = 4

#Solve
m.solve()


#%% Plot results
time = [x * 12 for x in m.time] 

# plot results
import matplotlib.pyplot as plt
plt.figure(1)

plt.subplot(311)
plt.plot(time,h[0].value,'r-')
plt.plot(time,h[1].value,'b--')
plt.ylabel('Level (m)')
plt.legend(['Jordanelle Reservoir','Deer Creek Reservoir'])

plt.subplot(312)
plt.plot(time,h[3].value,'g-')
plt.plot(time,h[2].value,'k:')
plt.ylabel('Level (m)')
plt.legend(['Great Salt Lake','Utah Lake'])

plt.subplot(313)
plt.plot(time,Vin[0].value,'k-')
plt.plot(time,Vout[0].value,'r-')
plt.plot(time,Vout[1].value,'b--')
plt.plot(time,Vout[2].value,'g-')
plt.xlabel('Time (month)')
plt.ylabel('Flow (km3/yr)')
plt.legend(['Supply Flow','Upper Provo River','Lower Provo River','Jordan River'])
plt.show()



"""
m.time = np.linspace(0,90,91)
Constants
  ! outflow constants
  c[1] = 0.03
  c[2] = c[1] / 2
  c[3] = c[1] * 2
  c[4] = 0

  ! usage amounts (km^3/yr)
  Vuse[1] = 0.03
  Vuse[2] = 0.05
  Vuse[3] = 0.02
  Vuse[4] = 0.00
  
Parameters
  ! evaporation constants
  evap_c[1:3] = 1e-5   ! fresh water
  evap_c[4]   = 0.5e-5 ! salt water

  ! surface areas
  A[1] = 13.4  ! km^2
  A[2] = 12.0  ! km^2
  A[3] = 384.5 ! km^2
  A[4] = 4400  ! km^2
  
  ! snow pack run-off
  Vin[1] ! see data file
  
Variables
  ! initial volumes (km^3)
  V[1] = 0.26 ! km^3
  V[2] = 0.18 ! km^3
  V[3] = 0.68 ! km^3
  V[4] = 22.0 ! km^3
  ! initial heights (m)
  h[1:4] = 1000 * V[1:4] / A[1:4]
  ! outlet flow rates (km^3/yr)
  Vout[1:4] = c[1:4] * sqrt(h[1:4])
  
Intermediates  
  ! river flow rates (km^3/yr)
  Vin[2:4] = Vout[1:3]
  ! evaporation rates (km^3/yr)
  Vevap[1:4] = evap_c[1:4] * A[1:4]
  
Equations
  $V[1:4] = Vin[1:4] - Vout[1:4] - Vevap[1:4] - Vuse[1:4]
  1000 * V[1:4] = h[1:4] * A[1:4]
  Vout[1:4]^2 = c[1:4]^2 * h[1:4]
  
  
  
  
  
time,Vin[1]
0,0.13
0.083333333,0.13
0.166666667,0.13
0.25,0.21
0.333333333,0.21
0.416666667,0.21
0.5,0.13
0.583333333,0.13
0.666666667,0.13
0.75,0.13
0.833333333,0.13
0.916666667,0.13
1,0.13



"""