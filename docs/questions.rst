.. _questions:

Dear Dr Hedengren,
==================


What are the secrets of APM?

.. toctree::
	:maxdepth: 2



Variable Classifications
========================

Constants
---------

Always constant.

Questions
^^^^^^^^^

Can they be an array? Are they read from the csv?


Parameters
----------

FV and MV are listed in Parameter section. Regular parameters are just constants. They can be set in the model file (single value) or csv for a array of values.

Questions
^^^^^^^^^

What is the benefit of a regular parameter (not FV or MV) over a constant?


Variables
---------

Variables are changed by the solver, they provide DOF. They can be initialized by the .apm or .csv. Variables can be differentials ($v = dv/dt).

Questions
^^^^^^^^^


Intermediates and their Equations
---------------------------------

Their values and gradients are set at the beginning of the iteration and don't change with solver function calls for that iterations, including line search, etc. This speeds up function call time, possbily at the expense of more iterations. Constant values could also provide some stability for the solver during the iteration, but could throw it off at when recalcuated at the next iteration. Should be used wisely. Intermediate values are clipped.

Questions
^^^^^^^^^

What value are they clipped at? How do they know when the next iteration is to resolve?

Equations
---------

$ is differential

Available functions: abs, sin, cos, tan, sinh, cosh, tanh

FV
--

Questions
^^^^^^^^^

Is this a "Fixed Variable" or a "Feedforward Variable"?
MV
--

SV
--

CV
--


IMODES
======

Mode 1 - SS Simulation
----------------------

Differentials = 0

Solve the set of algebraic equations.


Mode 2 - SS Estimation (MPU)
----------------------------

FV
^^

MV
^^

SV
^^

CV
^^

CVs are the measurement you want the model to match. Turning on FSTATUS and STATUS add ``min (ymodel - ymeas)`` to the objective function, with either l1 or l2 norm as otherwise specified.

Implied Objective
^^^^^^^^^^^^^^^^^


Mode 3 - SS Optimization (RTO)
------------------------------

FV
^^

MV
^^

SV
^^

CV
^^

Implied Objective
^^^^^^^^^^^^^^^^^


Mode 4/7 - Dynamic Simulation (ODE Solver)
------------------------------------------

FV
^^

MV
^^

SV
^^

CV
^^

Implied Objective
^^^^^^^^^^^^^^^^^


Mode 5/8 - Dynamic Estimation (MHE)
-----------------------------------

FV
^^

MV
^^

SV
^^

CV
^^

Implied Objective
^^^^^^^^^^^^^^^^^


Mode 6/9 - Dynamic Control (MPC)
---------------------------------

FV
^^

MV
^^

SV
^^

CV
^^

Implied Objective
^^^^^^^^^^^^^^^^^


Simultaneous vs Sequential
==========================

Simultaneous uses orthogonal collocation of finite elements. All variables are discretized according to time variable. All variables are optimized at the same time.

Sequential solves differentials using Runga-Kutta 4th order? All dependant variables are solved with whatever time discretization RK determines. MVs are discretized according to time variable. Each sequential MV step is solved to optimal, with associated dependent simulation, before moving on to the next time step.


Files
=====

CSV
---

Do all the columns always have to be the same length?

Info
----

dbs
---

measurements.dbs
----------------

What's the deal here? 


t0
--

How does the initialization from the t0 file get shifted with new data?

What is the priority for initializing variables between apm, csv and t0?

